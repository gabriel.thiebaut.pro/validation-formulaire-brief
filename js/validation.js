// Récupérer les elélements du DOM :
    const formbut       = document.querySelector("#submit");
    const input_name    = document.querySelector("#name");
    const input_email   = document.querySelector("#email");
    const input_phone   = document.querySelector("#phone");

// Variables Regex :
    const regex_phone   = /^(?:(?:\+|00)33|0)\s*[6-7](?:[\s.-]*\d{2}){4}$/;
    const regex_mail    = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

// Création des messages d'erreurs :
    // Création du message d'erreur de Name :
        const errorName = document.createElement("span");
        errorName.textContent = "Erreur : Le nom ne peut pas être vide et ne doit pas dépasser 50 caractères !";
        errorName.style.display="none";
        errorName.style.color="white";
        errorName.style.padding="8px";
        errorName.style.background="red";
        errorName.style.borderRadius="5px";
        errorName.style.marginTop="16px";
        input_name.parentElement.appendChild(errorName);

    // Création du message d'erreur de Email :
        const errorEmail = document.createElement("span");
        errorEmail.textContent = "Erreur : Adresse Email invalide !";
        errorEmail.style.display="none";
        errorEmail.style.color="white";
        errorEmail.style.padding="8px";
        errorEmail.style.background="red";
        errorEmail.style.borderRadius="5px";
        errorEmail.style.marginTop="16px";
        input_email.parentElement.appendChild(errorEmail);

    // Création du message d'erreur de Phone :
        const errorPhone = document.createElement("span");
        errorPhone.textContent = "Erreur : Le numéro de téléphone ne doit pas avoir d'espaces et doit commencer par 06 ou 07 !";
        errorPhone.style.display="none";
        errorPhone.style.color="white";
        errorPhone.style.padding="8px";
        errorPhone.style.background="red";
        errorPhone.style.borderRadius="5px";
        errorPhone.style.marginTop="16px";
        input_phone.parentElement.appendChild(errorPhone);

// Vérifications individuelles de chaque input :
    // Vérification pour l'input Name :
        input_name.addEventListener("keyup", (e) => {
            if (e.target.value.length <= 0 || e.target.value.length >= 50) {
                input_name.setAttribute("style", "border: 1px solid red;");
                errorName.style.display="block";
            } else {
                input_name.setAttribute("style", "border: 1px solid green;");
                errorName.style.display="none";
            }
        });

    // Vérification pour l'input Mail :
        input_email.addEventListener("keyup", (e) => {
            if (!regex_mail.test(input_email.value.toLowerCase())|| e.target.value == "") {
                input_email.setAttribute("style", "border: 1px solid red;");
                errorEmail.style.display="block";
            } else {
                input_email.setAttribute("style", "border: 1px solid green;");
                errorEmail.style.display="none";
            }
        });

    // Vérification pour l'input Phone :
        input_phone.addEventListener("keyup", (e) => {
            if (e.target.value.length != 10 || !regex_phone.test(e.target.value)) {
                input_phone.setAttribute("style", "border: 1px solid red;");
                errorPhone.style.display="block";
            } else {
                input_phone.setAttribute("style", "border: 1px solid green;");
                errorPhone.style.display="none";
            }
        });

// Vérification de tout les inputs avec le bouton submit :
        formbut.addEventListener('click', (event) => {
            // Vérification pour l'input Name :
                if (input_name.value.length <= 0 || input_name.value.length > 50) {
                    input_name.setAttribute("style", "border: 1px solid red;");
                    errorName.style.display="block";
                } else {
                    input_name.setAttribute("style", "border: 1px solid green;");
                    errorName.style.display="none";
                }

            //Vérification pour l'input Mail :
                if (!regex_mail.test(input_email.value.toLowerCase()) || input_email.value == "") {
                    input_email.setAttribute("style", "border: 1px solid red;");
                    errorEmail.style.display="block";
                } else {
                    input_email.setAttribute("style", "border: 1px solid green;");
                    errorEmail.style.display="none";
                }

            // Vérification pour l'input Phone :
                if (input_phone.value.length != 10 || !regex_phone.test(input_phone.value)) {
                    input_phone.setAttribute("style", "border: 1px solid red;");
                    errorPhone.style.display="block";
                } else {
                    input_phone.setAttribute("style", "border: 1px solid green;");
                    errorPhone.style.display="none";
                }
                event.preventDefault();
        })



// Développé par Gabriel Thiebaut pour le brief "Validation de formulaire".